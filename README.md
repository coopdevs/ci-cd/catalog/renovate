# Renovate CI/CD component

CI/CD Component that runs [`Renovate`](https://github.com/renovatebot/renovate).

You will have to provide your project with a [configuration file](https://docs.renovatebot.com/configuration-options/) (refer to this project's own file for a simple reference).

It is also necessary to set the Gitlab CI/CD Variable `RENOVATE_TOKEN` to an access token with API and Repository rights ([docs](https://docs.renovatebot.com/modules/platform/gitlab/#authentication)).

## Usage

Add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - component: ${CI_SERVER_HOST}/northamp/cicd-components/renovate/renovate@~latest
```

Replace `~latest` to a specific tag if you desire.

It's also possible to use `rules` and `inputs` to decide when to run the job, and define some parameters.

```yaml
include:
  - component: ${CI_SERVER_HOST}/northamp/cicd-components/renovate/renovate@~latest
    inputs:
      stage: .post
    rules:
      - if: $CI_PIPELINE_SOURCE == "schedule" && $GCI_RENOVATE
```

### Inputs

| Input          | Default value       | Description                                                     |
| -------------- | ------------------- | --------------------------------------------------------------- |
| `stage`        | `renovate`          | The stage where you want the job to be added                    |
| `image_prefix` | `renovate/renovate` | Image that should be used by the job in format `registry/image` |
| `image_tag`    | `latest`            | Tag of the image, will be appended to `image_prefix`            |
| `dry_run`      | `""`                | Set it to `"true"` to run Renovate in dry-run mode              |
